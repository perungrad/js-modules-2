const extendByRange = function (unit) {
    let range;

    unit.setRange = function (newRange) {
        range = newRange;

        return unit;
    };

    unit.getRange = function () {
        return range;
    };

    return unit;
};
