const unitFactory = require('./unit/unitFactory.js');
const extendByRange = require('./unit/ranged.js');

const pikeman = unitFactory().setName('Pikeman').setSpeed(2).setStrength(3).setHealth(4);

const horseman = unitFactory().setName('Horseman').setSpeed(4).setStrength(4).setHealth(3);

const archer = extendByRange(unitFactory())
    .setName('Archer')
    .setSpeed(2)
    .setStrength(2)
    .setHealth(4)
    .setRange(4);

console.log({ pikeman });
console.log({ horseman });
console.log({ archer });
