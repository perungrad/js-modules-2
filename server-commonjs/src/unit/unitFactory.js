const unitFactory = function() {
    let name;
    let speed;
    let strength;
    let health;

    let unit = {};

    unit.setName = function(newName) {
        name = newName;

        return unit;
    };

    unit.getName = function() {
        return name;
    };

    unit.setSpeed = function(newSpeed) {
        speed = newSpeed;

        return unit;
    };

    unit.getSpeed = function() {
        return speed;
    };

    unit.setStrength = function(newStrength) {
        strength = newStrength;

        return unit;
    };

    unit.getStrength = function() {
        return strength;
    };

    unit.setHealth = function(newHealth) {
        health = newHealth;

        return unit;
    };

    unit.getHealth = function() {
        return health;
    };

    return unit;
};
